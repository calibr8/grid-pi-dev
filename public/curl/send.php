<?php
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);    
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);  
    curl_setopt($curl, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);    
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_GSSNEGOTIATE);    
    curl_setopt($curl, CURLOPT_URL, 'https://vision.pienergy-analytics.com/grid-pi/public/api/write-data');  
    $result = curl_exec($curl); 
    curl_close($curl);

    print_r($result);die;