<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGridSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grid_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 50);
            $table->string('password', 50);
            $table->string('direction', 50)->default('FORWARD');;
            $table->string('data_type', 50)->default('ENHANCE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grid_settings');
    }
}
