<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PIWebAPIController;
use App\MapTag;
use App\PiwebapiSetting;

class MapController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTagList() {
        $tags = MapTag::orderBy('id', 'ASC');
        return response()->json([
            'data' => $tags->get()
        ]);
    }

    public function saveTag(Request $request) {
        $params = json_decode($request->getContent());
        $piwebapi_settings = PiwebapiSetting::find(1);
        $piwebapi = new PIWebAPIController();
        $path = '\\\\' . $piwebapi_settings->pi_server . '\\' . $params->tag_name;
        // return $piwebapi_settings->url . '/points?path=' . urlencode($path);
        $tag_details = $piwebapi->getRequest($piwebapi_settings->url . '/points?selectedFields=webid&path=' . urlencode($path), $piwebapi_settings->username, $piwebapi_settings->password);
        if($tag_details['httpcode'] == 404) {
            $batch = [
                "1" => [
                    'Resource' => $piwebapi_settings->url . '/dataservers?name=' . $piwebapi_settings->pi_server . '&selectedFields=Links.Points',
                    'Method' => 'GET'
                ],
                "2" => [
                    'Resource' => '$.1.Content.Links.Points',
                    'Method' => 'POST',
                    'Content' => json_encode([
                        'Name' => $params->tag_name,
                        'PointType' => 'string',
                        'PointClass' => 'classic',
                        'Future' => true
                    ]),
                    'ParentIds' => ['1']
                ]

            ];
            $response = $piwebapi->postRequest($piwebapi_settings->url . '/batch', $piwebapi_settings->username, $piwebapi_settings->password, $batch);
            
            if($response['data']->{2}->Status == 201) {
                $tag_details = $piwebapi->getRequest($piwebapi_settings->url . '/points?selectedFields=webid&path=' . urlencode($path), $piwebapi_settings->username, $piwebapi_settings->password);
            }
            else {
                return response()->json([
                    'errors' => $response['data']->{2}->Content->Errors
                ], $response['data']->{2}->Status);
            }
        }
        
        if($params->id) {
            $tag = MapTag::find($params->id);            
        }
        else {
            $tag = new MapTag();
        }

        $tag->tag_name = $params->tag_name;
        $tag->web_id = $tag_details['data']->WebId;
        $tag->save();

        return response()->json($tag);
    }
    
    public function getTagValues($web_id) {
        $piwebapi = new PIWebAPIController();
        $piwebapi_settings = PiwebapiSetting::find(1);
        $tag_details = $piwebapi->getRequest($piwebapi_settings->url . '/streams/' . $web_id . '/recorded?selectedFields=items&startTime=*-5y', $piwebapi_settings->username, $piwebapi_settings->password);

        return response()->json($tag_details);
    }

    public function saveTagValue($web_id, Request $request) {
        $params = json_decode($request->getContent());
        $value = [
            'Timestamp' => gmdate('Y-m-d\TH:i:00\Z', strtotime($params->timestamp)),
            'Value' => $params->value
        ];

        $piwebapi = new PIWebAPIController();
        $piwebapi_settings = PiwebapiSetting::find(1);
        $tag_value = $piwebapi->postRequest($piwebapi_settings->url . '/streams/' . $web_id . '/value?startTime=*-5y', $piwebapi_settings->username, $piwebapi_settings->password, $value);

        return response()->json($tag_value);
    }

    public function deleteTag($id) {
        $tag = MapTag::find($id);
        $tag->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }
}
