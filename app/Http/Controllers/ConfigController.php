<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PiwebapiSetting;
use App\GridSetting;
use App\TagSetting;
use App\Transformer;
use App\Utility;
use App\Configuration;
use App\Feeder;
use App\Http\Controllers\PIWebAPIController;
use App\Http\Controllers\GridAPIController;
use DB;

class ConfigController extends Controller
{
    public function getPIConfigurations() {
		$piwebapi = PiwebapiSetting::find(1);
		$gridapi = GridSetting::find(1);
    	return response()->json(['piwebapi' => $piwebapi, 'gridapi' => $gridapi]);
    }

    public function checkPIConnection(Request $request) {
    	$piwebapi = new PIWebAPIController();
    	$piwebapi_details = $piwebapi->getRequest($request->url . '/dataservers', $request->username, $request->password);

		// $grid = new GridAPIController();
		// $grid_details = $grid->getRequest();

    	return response()->json($piwebapi_details);
    }

    public function savePIConnection(Request $request) {
    	if($request->id) {
    		$piwebapi_settings = PiwebapiSetting::find($request->id);
    		$piwebapi_settings->url = $request->url;
    		$piwebapi_settings->username = $request->username;
    		$piwebapi_settings->password = $request->password;
    		$piwebapi_settings->pi_server = $request->pi_server;
    		$piwebapi_settings->save();

    		return response()->json($piwebapi_settings);
    	}
    	else {
    		$piwebapi_settings = new PiwebapiSetting();
    		$piwebapi_settings->url = $request->url;
    		$piwebapi_settings->username = $request->username;
    		$piwebapi_settings->password = $request->password;
    		$piwebapi_settings->pi_server = $request->pi_server;
    		$piwebapi_settings->save();

    		return response()->json($piwebapi_settings);
    	}
	} 
	
	public function getTagConfigurations() {
		$transformers = Transformer::orderBy('xfid', 'asc');
		$pi_config = PiwebapiSetting::find(1)->select('id', 'pi_server');
		$utilities = Utility::orderBy('utility_id', 'ASC')->select('id', 'utility_id');
		$feeders = Feeder::orderBy('feeder_id', 'ASC')->select('id', 'feeder_id');
		$configurations = DB::Table('configurations as c')
			->leftJoin('utilities as u', 'u.id', '=', 'c.utility_id')
			->leftJoin('transformers as t', 't.id', '=', 'c.transformer_id')
			->leftJoin('feeders as f', 'f.id', '=', 'c.feeder_id')
			->select('c.*', 'u.utility_id as utility_id_str', 't.xfid', 'f.feeder_id as feeder_id_str');
		return response()->json([
				'pi_config' => $pi_config, 
				'configurations' => $configurations->get(),
				'transformers' => $transformers->get(), 
				'utilities' => $utilities->get(),
				'feeders' => $feeders->get()
			]);
	}

	public function getTagSettings($id, Request $request) {
		$tag_list = TagSetting::where('configuration_id', $id)->orderBy('id', 'ASC');
		return response()->json($tag_list->get());

	
	}

	public function getTagDetails(Request $request) {
		$pi_config = PiwebapiSetting::find(1);
		$path = '\\\\' . $pi_config->pi_server . '\\' .  $request->tag_name;

		$piwebapi = new PIWebAPIController();
		return $piwebapi->getRequest($pi_config->url . '/points?selectedFields=webid&path=' . $path, $pi_config->username, $pi_config->password);
	}

	public function validateTags(Request $request) {
		$request_params = json_decode($request->getContent());
		$pi_config = PiwebapiSetting::find(1);

		$params = [];
		foreach($request_params->tags as $k => $t) {
			$params[$t->idx + 1] = [
				'Resource' => $pi_config->url . '/points?selectedFields=webid&path=\\\\' . urlencode($pi_config->pi_server) . '\\' . urlencode($t->tag_name),
				'Method' => 'GET',
			];
		}

		$piwebapi = new PIWebAPIController();
		$piwebapi_response = $piwebapi->postRequest($pi_config->url . '/batch', $pi_config->username, $pi_config->password, $params);

		$tag_status = array();
		
		foreach($piwebapi_response['data'] as $idx => $r) {
			if($r->Status == 200) {
				$tag_status[$idx - 1] = [
					'status' => 200,
					'web_id' => $r->Content->WebId
				];
			}
			else {
				$tag_status[$idx - 1] = [
					'status' => $r->Status,
					'web_id' => ''
				];
			}
		}

		return response()->json($tag_status);
	}

	public function saveTagConfigurations($id, Request $request) {
		$params = json_decode($request->getContent());
		
		$response = [];
		$tag_ids = [];
		foreach($params->tags as $t) {
			if(isset($t->id)) {
				$tag_info = TagSetting::find($t->id);
			}
			else {
				$tag_info = new TagSetting();
			}
			$tag_info->configuration_id = $id;
			$tag_info->source_param = $t->source_param;
			$tag_info->tag_name = $t->tag_name;
			$tag_info->web_id = $t->web_id;
			$tag_info->save();

			$tag_ids[] = $tag_info->id;

			$response[] = $tag_info;
		}

		$deleted = TagSetting::where('configuration_id', $id)->whereNotIn('id', $tag_ids)->delete();
		
		return response()->json($response);
	}

	public function checkGridConnection(Request $request) {
		$grid = new GridAPIController();
		$start_time = date('Y-m-d\TH:i', strtotime('-15 minutes')) . '+0800';
		$end_time = date('Y-m-d\TH:i') . '+0800';

		$url = env('GRID_URL') . '/getXFcsv.jsp?UTILITY='. $request->utility_id .'&XFID=1000&STARTTIME='. $start_time .'&ENDTIME='. $end_time .'&DIRECTION='. $request->direction .'&DATATYPE=ENHANCED&USER='. $request->username .'&PASSWORD=' . $request->password;
		$filename = 'tester';
		return $grid->getRequest($url, $filename);
	}

	public function saveGridConnection(Request $request) {
		if($request->id) {
			$grid_settings = GridSetting::find($request->id);
		}
		else {
			$grid_settings = new GridSetting();
		}

		$grid_settings->username = $request->username;
		$grid_settings->password = $request->password;
		$grid_settings->direction = $request->direction;
		$grid_settings->save();

		return response()->json($grid_settings);
	}

	public function addTransformer(Request $request) {
		$params = json_decode($request->getContent());
		$transformer = new Transformer;
		$transformer->xfid = $params->xfid;
		$transformer->save();

		return response()->json($transformer);
	}

	public function deleteTranformer($id) {
		$transformer = Transformer::find($id);
		$transformer->delete();

		return response()->json(['status' => 'success']);
	}

	public function addUtility(Request $request) {
		$params = json_decode($request->getContent());
		$utility = new Utility();
		$utility->utility_id = $params->utility_id;
		$utility->save();

		return response()->json($utility);
	}

	public function deleteUtility($id) {
		$utility = Utility::find($id);
		$utility->delete();

		return response()->json(['status' => 'success']);
	}

	public function addFeeder(Request $request) {

	}

	public function deleteFeeder($id) {
		$feeder = Feeder::find($id);

		$feeder->delete();

		return response()->json(['status' => 'success']);
	}

	public function saveConfiguration(Request $request) {
		$params = json_decode($request->getContent());

		if($params->id) {
			$configuration = Configuration::find($params->id);
		}
		else {
			$configuration_exists = Configuration::where('utility_id', $params->utility_id)
			->where('feeder_id', $params->feeder_id)
			->where('transformer_id', $params->transformer_id)->first();
		
			if(is_object($configuration_exists)) {
				return response()->json(['status' => 'failed', 'message' => 'Configuration Details Already Exists'], 400);
			}

			$configuration = new Configuration();
		}

		$configuration->description = $params->description;
		$configuration->utility_id = $params->utility_id;
		$configuration->transformer_id = $params->transformer_id;
		$configuration->feeder_id = $params->feeder_id;
		$configuration->enabled = $params->enabled;
		$configuration->save();

		$configuration_details = DB::Table('configurations as c')
			->leftJoin('utilities as u', 'u.id', '=', 'c.utility_id')
			->leftJoin('transformers as t', 't.id', '=', 'c.transformer_id')
			->leftJoin('feeders as f', 'f.id', '=', 'c.feeder_id')
			->select('c.*', 'u.utility_id as utility_id_str', 't.xfid', 'f.feeder_id as feeder_id_str');

		return response()->json(['status' => 'success', 'data' => $configuration_details->get()]);
	}

	public function deleteConfiguration($id) {
		$configuration = Configuration::find($id);
		$configuration->delete();

		return response()->json(['status' => 'success']);
	}
}
