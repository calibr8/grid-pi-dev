<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PIWebAPIController;
use App\TagSetting;
use App\GridSetting;
use App\PiwebapiSetting;
use Storage;
use stdClass;
use DateTime;
use DB;

class GridAPIController extends Controller
{
    public function test() {
        $pi_config = PiwebapiSetting::find(1);
        $grid_config = GridSetting::find(1);
        $start_time = '2021-03-08T14:45' . '+0800';
        $end_time = '2021-03-08T15:00' . '+0800';

        $utilities = DB::table('utilities as u')->leftJoin('transformers as t', 't.utility_id', '=', 'u.id')->select('u.utility_id', 't.xfid');
        $response  = [];
        foreach($utilities->get() as $u) {
            $url = env('GRID_URL') . '/getXFcsv.jsp?UTILITY='. $u->utility_id .'&XFID='. $u->xfid .'&STARTTIME='. $start_time .'&ENDTIME='. $end_time .'&DIRECTION='. $grid_config->direction .'&DATATYPE=ENHANCED&USER='. $grid_config->username .'&PASSWORD=' . $grid_config->password;
            
            $filename = date('Y-m-d-Hi', strtotime('+8 hours'));
            $file = $this->getRequest($url, $filename);
            $data = $this->csvToArray(Storage::path('csv/' . $file));
			
            $piwebapi = new PIWebAPIController();
            $response[] = [
				'request_url' => $url,
                'utility_id' => $u->utility_id,
                'xfid' => $u->xfid,
                'data' => $data,
                'response' => $piwebapi->writeData($data)
            ];
        }
		
		return response()->json($response);

    }

    public function csvToArray($path) {
        $csv = array_map('str_getcsv', file($path));
		
		if(!count($csv)) {
			$data = new stdClass();
			$data->tag_list = [];
			$data->values = [];

			return $data;
		}
		
        $tag_list = array_slice($csv[0], 4);
        $tag_values = [];

        foreach($csv as $idx => $c) {
            if($idx > 0) {
                $timestamp = gmdate('Y-m-d\TH:i:s\Z', strtotime($c[0]));
                $values_arr = array_slice($c, 4);
                foreach($values_arr as $key => $value) {
                    $tag_values[$tag_list[$key]][] = [
                        'Timestamp' => $timestamp,
                        'Value' => $value
                    ];
                }
            }
        }

        $data = new stdClass();
        $data->tag_list = $tag_list;
        $data->values = $tag_values;

        return $data;
    }

    public function getData(Request $request) {
        $gridsettings = GridSetting::find(1);

        $grid = new GridAPIController();
		$start_time = date('Y-m-d\TH:i:00', strtotime($request->from)) . '+0800';
        $end_time = date('Y-m-d\TH:i:00', strtotime($request->to)) . '+0800';

		$url = env('GRID_URL') . '/getXFcsv.jsp?UTILITY='. $request->utility .'&XFID='. $request->transformer . '&feeder_id=' . $request->feeder .'&STARTTIME='. $start_time .'&ENDTIME='. $end_time .'&DIRECTION='. $gridsettings->direction .'&DATATYPE=ENHANCED&USER='. $gridsettings->username .'&PASSWORD=' . $gridsettings->password;

        $filename = date('Y-m-d-0000');
		$file = $grid->getRequest($url, $filename);
        $csv = array_map('str_getcsv', file(Storage::path('csv/' . $file)));
        return response()->json($csv);
    }

    public function backFill(Request $request) {
        $params = json_decode($request->getContent());
        $data = $params->data;
        $headers = $params->headers;
        $tag_list = array_slice($headers, 4);
        $tag_values = [];

        foreach($data as $idx => $c) {
            $timestamp = gmdate('Y-m-d\TH:i:s\Z', strtotime($c[0]));
            $values_arr = array_slice($c, 4);
            foreach($values_arr as $key => $value) {
                $tag_values[$tag_list[$key]][] = [
                    'Timestamp' => $timestamp,
                    'Value' => $value
                ];
            }
        }

        $data = new stdClass();
        $data->tag_list = $tag_list;
        $data->values = $tag_values;

        $piwebapi = new PIWebAPIController();
        return response()->json($piwebapi->writeData($data));
    }

    public function loadSampleData(Request $request) {
        $grid = GridSetting::find(1);

        $start_time = date('Y-m-d\T01:00', strtotime('-15 minutes')) . '+0800';
		$end_time = date('Y-m-d\T01:15') . '+0800';

        $url = env('GRID_URL') . '/getXFcsv.jsp?UTILITY='. $request->utility_id 
            . '&XFID='. $request->transformer_id 
            . '&STARTTIME='. $start_time 
            . '&ENDTIME='. $end_time 
            . '&DIRECTION='. $grid->direction 
            . '&DATATYPE=ENHANCED&USER='. $grid->username 
            . '&PASSWORD=' . $grid->password;
        $url = $request->feeder_id ? $url . '&FEEDERID=' . $request->feeder_id : $url;

		$filename = 'tester';
        $file = $this->getRequest($url, $filename);
        $data = $this->csvToArray(Storage::path('csv/' . $file));

        return response()->json($data);
    }

    public function createTags(Request $request) {
        $params = json_decode($request->getContent());
        $piconfig = PiwebapiSetting::find(1);
        $piwebapi = new PIWebAPIController();

        $data_server = $piwebapi->getRequest($piconfig->url . '/dataservers?selectedFields=WebId&name=' . urlencode($piconfig->pi_server), $piconfig->username, $piconfig->password);
		$batch = [];

        foreach($params->tags as $t) {
            $tag_info = [
                'Name' => $t->tag_name,
                'PointType' => 'Float32',
                'PointClass' => 'classic',
            ];
            $batch[$t->source_param] = [
                'Resource' => $piconfig->url . '/dataservers/' . $data_server['data']->WebId . '/points',
                'Method' => 'POST',
                'Content' => json_encode($tag_info)
            ];
        }

        $response = $piwebapi->postRequest($piconfig->url . '/batch', $piconfig->username, $piconfig->password, $batch);
    
        return response()->json($response);
    }

    public function getRequest($url, $filename) {
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);    
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);  
		curl_setopt($curl, CURLOPT_GSSAPI_DELEGATION, CURLGSSAPI_DELEGATION_FLAG);    
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_GSSNEGOTIATE);    
		curl_setopt($curl, CURLOPT_URL, $url);
		$fp = fopen(Storage::path('csv/'. $filename .'.csv'), 'w');
		curl_setopt($curl, CURLOPT_FILE, $fp);
		curl_exec ($curl);
		curl_close ($curl);
		fclose($fp);
	
        return $filename . '.csv';
    }

    public function writeDatatoPI() {
        $pi_config = PiwebapiSetting::find(1);
        $grid_config = GridSetting::find(1);
        $start_time = date('Y-m-d\TH:i', strtotime('-16 minutes')) . '+0800';
        $end_time = date('Y-m-d\TH:i', strtotime('-1 minutes')) . '+0800';

        $configurations = DB::table('configurations as c')
            ->leftJoin('utilities as u', 'u.id', '=', 'c.utility_id')
            ->leftJoin('transformers as t', 't.id', '=', 'c.transformer_id')
            ->leftJoin('feeders as f', 'f.id', '=', 'c.feeder_id')
            ->where('c.enabled', 1)
            ->select('c.*', 'u.utility_id as utility_id_str', 't.xfid', 'f.feeder_id as feeder_id_str');

        $response  = [];
        foreach($configurations->get() as $c) {
            $url = env('GRID_URL') 
                . '/getXFcsv.jsp?UTILITY='. $c->utility_id_str 
                . '&XFID=' . $c->xfid 
                . '&STARTTIME=' . $start_time 
                . '&ENDTIME=' . $end_time 
                . '&DIRECTION=' . $grid_config->direction 
                . '&DATATYPE=ENHANCED&USER='. $grid_config->username 
                . '&PASSWORD=' . $grid_config->password;
            
            $url = $c->feeder_id_str ? $url . '&FEEDERID=' . $c->feeder_id_str : $url;

            $filename = date('Y-m-d-Hi', strtotime('+8 hours'));
            $file = $this->getRequest($url, $filename);
            $data = $this->csvToArray(Storage::path('csv/' . $file));

            $piwebapi = new PIWebAPIController();
            $response[] = [
				'request_url' => $url,
                'utility_id' => $u->utility_id,
                'xfid' => $u->xfid,
                'data' => $data,
                'response' => $piwebapi->writeData($data)
            ];
        }

        Storage::disk('local')->put('logs/' . date('Y-m-d-Hi') . '.json', json_encode($response));
        return response()->json($response);

    }
    
}
