<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Firebase\JWT\JWT;

class LDapController extends Controller
{
    //
    protected $host = 'pi-ad.pienergy.com';
    protected $key = "calibr8Systems";

    function test() {
        $ldaphost = "pi-ad.pienergy.com";

        // Connecting to LDAP
        $ldap = ldap_connect($ldaphost)
                  or die("That LDAP-URI was not parseable");
		
		if ($bind = ldap_bind($ldap, 'Administrator@pienergy.com', 'C8@dm1n')) {
			$search = ldap_search($ldap, 'CN=Users,DC=PIENERGY,DC=com', "(&(objectClass=user)(sAMAccountName=administrator)(memberof=CN=Grid2020,CN=Users,DC=PIENERGY,DC=com))");
			//$search = ldap_search($ldap, 'CN=Users,DC=PIENERGY,DC=com', "(samaccountname=administrator)",['memberof']);
			if ($search) {
                $result = ldap_get_entries($ldap, $search);
                if ($result['count'] > 0) {
                    $returnval = 1; // "Success"
					print_r($result);
                }
                else {
                    $returnval = -1; // "User not found"
                }
            }
		  // log them in!
		} else {
			
		}
    }
	
	function login(Request $request) {
		$params = json_decode($request->getContent());
		$ldap = ldap_connect($this->host)
                  or die("That LDAP-URI was not parseable");
		
		$username = strpos($params->username, '@pienergy.com') ? $params->username : $params->username . '@pienergy.com';
		$smaaccount = explode('@', $username)[0];
		try	{
			if ($bind = ldap_bind($ldap, $username, $params->password)) {
				$search = ldap_search($ldap, 'CN=Users,DC=PIENERGY,DC=com', '(&(objectClass=user)(sAMAccountName='. $smaaccount .')(memberof=CN=Grid2020,CN=Users,DC=PIENERGY,DC=com))',['memberof']);
				if ($search) {
					$result = ldap_get_entries($ldap, $search);
					if ($result['count'] > 0) {
                        $jwt = JWT::encode(['username' => $username, 'password' => $params->password], $this->key);
						return response()->json([
							'status' => 'success',
							'data' => ['token' => $jwt]
						], 200);
					}
					else {
						return response()->json([
							'status' => 'failed',
							'message' => 'Access Denied'
						], 403);
					}
				}
			  // log them in!
			}
		}
		catch(Exception $e) {
			return response()->json([
							'status' => 'failed',
							'message' => 'Invalid Credentials'
						], 403);
		} 
	}
}
